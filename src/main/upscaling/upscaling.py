import os
import argparse
from PIL import Image
import numpy as np
from ISR.models import RDN


class Upscaler:
    """Increases resolution of images"""

    def __init__(self):
        """TODO DOC"""
        self.upscaler = RDN(arch_params={'C': 6, 'D': 20, 'G': 64, 'G0': 64, 'x': 2})
        self.upscaler.model.load_weights('../models/rdn-C6-D20-G64-G064-x2_ArtefactCancelling_epoch219.hdf5')

    def image_to_high_res(self, image_path, output_path=None):
        """DOC"""
        loaded_image = Image.open(image_path)
        lr_img = np.array(loaded_image)
        sr_img = self.upscaler.predict(lr_img)
        upres_img = Image.fromarray(sr_img)
        if not output_path:
            output_path = os.path.normpath(
                            os.path.join(os.path.split(
                                image_path)[0], 'HIGH_RES---'+os.path.split(image_path)[1]))
        upres_img.save(output_path)
        print('High res {} saved to'.format(os.path.basename(image_path)), output_path)

    def upscale_image(self, image_path, output_path=None, scale_ratio=5, mode='noise_scale',
                      noise_level=2, tta=0, process='cudnn'):
        """DOC"""
        # Params for upscaling
        SCALE_RATIO = scale_ratio
        MODE = mode                     # < noise | scale | noise_scale >
        NOISE_LEVEL = noise_level       # < 0 | 1 | 2 | 3 >
        TTA = tta                       # < 0 | 1 > 8x slower and slightly higher quality
        PROCESS = process               # <cpu | gpu | cudnn>

        os.chdir('../upscaling')
        if not output_path:
            output_path = os.path.normpath(
                            os.path.join(os.path.split(
                                image_path)[0], 'UPSCALED---'+os.path.split(image_path)[1]))

        print('\nUpscaling image {}...'.format(os.path.basename(image_path)))
        print(os.path.split(output_path)[-1])
        cmd = "powershell.exe .\waifu2x-caffe\waifu2x-caffe-cui.exe --input_path '{}' --output_path '{}' --mode {} --scale_ratio {} --noise_level {} --tta {} --process {}".format(
                    os.path.normpath(os.path.abspath(image_path)), os.path.normpath(os.path.abspath(output_path)),
                    MODE, SCALE_RATIO, NOISE_LEVEL, TTA, PROCESS)
        os.system(cmd)
        print('Output saved to {}'.format(output_path))
        os.chdir('../src')


def main(image_path, high_res_iter, upscale_zoom=None):
    upscaler = Upscaler()
    HIGH_RES_PREFIX = 'HIGH_RES'
    output_path = os.path.normpath(
        os.path.join(os.path.split(
            image_path)[0], HIGH_RES_PREFIX + '---' + os.path.split(image_path)[1]))

    if high_res_iter:
        # first high res iteration
        print('High res iteration 1...')
        upscaler.image_to_high_res(image_path=image_path,
                                   output_path=output_path.replace(HIGH_RES_PREFIX, HIGH_RES_PREFIX + '1'))
        # other high res iterations
        for i in range(high_res_iter - 1):
            print('High res iteration {}...'.format(i + 2))
            upscaler.image_to_high_res(image_path=output_path.replace(HIGH_RES_PREFIX, HIGH_RES_PREFIX + str(i + 1)),
                                       output_path=output_path.replace(HIGH_RES_PREFIX, HIGH_RES_PREFIX + str(i + 2)))
    else:
        print('Skipping increased resolution step...')

    if upscale_zoom:
        if high_res_iter > 0:
            upscaler.upscale_image(image_path=output_path, output_path=output_path, scale_ratio=upscale_zoom)
        else:
            upscaler.upscale_image(image_path=image_path, output_path=output_path, scale_ratio=upscale_zoom)
    else:
        print('Skipping upscale zoom step...')

    print('Finished.')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Upscaler to make image ready for sale.')
    parser.add_argument("--image_path", dest="image_path", type=str, help="Full path to input image.")
    parser.add_argument("--high_res_iterations", dest="high_res_iter", type=int,
                        help="Num times to apply high res model. More than twice is not recommended.")
    parser.add_argument("--upscale_zoom", dest="upscale_zoom", type=float, help="Zoom factor for upscaling model.")

    args = parser.parse_args()
    image_path = args.image_path
    high_res_iter = args.high_res_iter
    upscale_zoom = args.upscale_zoom

    try:
        os.chdir('./src')
    except:
        pass

    main(image_path, high_res_iter, upscale_zoom)

# example: python src/upscaling.py --image_path "C:\Users\klein535\Google Drive\Neural_kunst_project\Output\Selection\Groningen\Expressionisme\STYLE_EXPRESSIONISME--SOUTINE-Karkas-van-een-rund_CONTENT_martinitoren_COLOR_False_IMSIZE_400_first.png" --high_res_iterations 2
