import os
from os import listdir
from os.path import isfile, join

from PIL import Image
import numpy as np

from main.upscaling import upscaling
from main.color_transfer import color_transfer

BASE_PATH = os.path.normpath("../data/selection/to_process/")
paths = [os.path.abspath(join(BASE_PATH, f)) for f in listdir(BASE_PATH) if isfile(join(BASE_PATH, f))]
SCALE_FACTOR = 2  # do not edit
MAX_DIM = 16000  # set maximum allowed output dimension (so max width or height)

for full_image_path in paths:
    image_path = os.path.basename(full_image_path)
    print('Processing:', image_path)

    # Find current dimensions of this image
    img = np.array(Image.open(full_image_path))
    max_dim = max(img.shape)

    # Calculate maximum allowed high iterations (to not exceed MAX_DIM constraint)
    high_res_iter = int(np.log(MAX_DIM / max_dim) / np.log(SCALE_FACTOR))
    print('Running {} iteration(s) for this image.'.format(high_res_iter))

    # Apply upscaling
    try:
        image_path = os.path.join(BASE_PATH, image_path)
        upscaling.main(image_path, high_res_iter)

        # Apply color transfer to preserve original color tone
        for iter in range(high_res_iter):
            high_res_path_name = os.path.abspath(os.path.join(BASE_PATH,
                                                 os.path.normpath(f"HIGH_RES{iter+1}---" + os.path.basename(full_image_path))))
            print('Applying color transfer on {}'.format(os.path.basename(high_res_path_name)))
            cmd = "python color_transfer.py \"{}\" \"{}\"".format(full_image_path, high_res_path_name)
            try:
                color_transfer.main(full_image_path, high_res_path_name)
                os.remove(high_res_path_name)
            except Exception as e:
                print('Color transfer failed for', os.path.basename(high_res_path_name))
                print('Exception:', e)
    except Exception as e:
        print(f'\nFailed to upscale {image_path}\n')
