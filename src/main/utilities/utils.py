import os

from PIL import Image


def convert_file_to_jpg(full_file_path):
    _, ext = os.path.splitext(full_file_path)
    im = Image.open(full_file_path)
    rgb_im = im.convert('RGB')
    rgb_im.save(full_file_path.replace(ext, '.jpg'))
