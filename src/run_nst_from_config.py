import os
import yaml
import time

from main.upscaling import upscaling
from main.utilities import utils


def load_pictures_to_paint(CONFIG_FOLDER):
    """DOC"""
    pictures_to_paint = [doc for doc in os.listdir(CONFIG_FOLDER) if os.path.splitext(doc)[-1].lower() == '.yaml']
    return pictures_to_paint


def paint_from_config_params(config_params):
    """DOC"""
    APPLY_MASK = config_params['apply_mask']
    if APPLY_MASK == 'True':
        COLOR_PRESERVATION = ['False', 'True']
    else:
        COLOR_PRESERVATION = config_params['preserve_color']
        if type(COLOR_PRESERVATION) is not list:
            COLOR_PRESERVATION = [COLOR_PRESERVATION]

    for col in COLOR_PRESERVATION:
        content_img = config_params['content_image']
        style_img = config_params['style_image']

        style = os.path.splitext(style_img)[0]
        content = os.path.normpath(os.path.splitext(content_img)[0]).split('\\')[-1]
        print('Painting {} using style {} and color preservation set to {}...'.format(content,
                                                                                      style,
                                                                                      col))

        if not os.path.exists("../data/output/{}".format(content)):
            os.mkdir("../data/output/{}".format(content))
            os.mkdir("../data/output/{}/final_images".format(content))

        content_weight = config_params['content_weight']
        style_weight = config_params['style_weight']
        iterations = config_params['iterations']
        image_size = config_params['image_size']
        init_image = config_params['init_image']
        pool_type = config_params['pool_type']
        try:
            apply_upscaling = config_params['apply_upscaling']
        except:
            apply_upscaling = None
        preserve_color = col
        output_name = 'STYLE_{}_CONTENT_{}_COLOR_{}_IMSIZE_{}'.format(utils.remove_slashes_from_path(style), content, col, image_size)

        cmd = "python ./main/style_transfer/inetwork.py ../data/inputs/content/{} ../data/inputs/style/{} ../data/output/{}/{} --image_size {} --content_weight {} --style_weight {} --num_iter {} --init_image {} --pool_type {} --preserve_color {}".format(
            content_img, style_img, content, output_name, image_size, content_weight, style_weight, iterations,
            init_image, pool_type,
            preserve_color)
        os.system(cmd)

        # ... make the first and final image higher res
        if apply_upscaling:
            print('\nIncreasing resolution...')
            upscaler = upscaling.Upscaler()
            output_path = '../data/output/{}/{}'.format(content, output_name)
            first_image = os.path.split(output_path)[0] + '/final_images/' + os.path.split(output_path)[
                1] + "_first.png"
            final_image = os.path.split(output_path)[0] + '/final_images/' + os.path.split(output_path)[
                1] + "_final.png"
            for img in [first_image, final_image]:
                if os.path.isfile(img):
                    # set output path to same as input path; overwrite low res file by high res
                    upscaler.image_to_high_res(img, output_path=img)
        else:
            print('\nSkipping increase resolution step...')

    if APPLY_MASK:
        MASK_PATH = '../data/inputs/mask/{}'.format(content + '_mask.jpg')
        if os.path.exists(MASK_PATH):
            try:
                print('Applying face mask...')
                apply_mask_cmd = "python mask_transfer.py ../data/output/{}/final_images/{} ../data/output/{}/final_images/{} ../data/inputs/mask/{}".format(
                    content,
                    output_name.replace('False', 'True') + '_final.png',
                    content,
                    output_name.replace('True', 'False') + '_final.png',
                    content + '_mask.jpg')
                os.system(apply_mask_cmd)
            except Exception as e:
                print(e)
        else:
            print('No mask file found for {}!\n'.format(content_img))


CONFIG_FOLDER = '../config_files/to_paint'

pictures_to_paint = load_pictures_to_paint(CONFIG_FOLDER=CONFIG_FOLDER)

assert pictures_to_paint, 'No pictures loaded! Specify pictures to paint in ../config_files/config.yaml'

SLEEP_IN_BETWEEN_PICTURES = False
SLEEP_TIME_SECONDS = 60

# TODO add upscaling to pipeline (final image), https://github.com/idealo/image-super-resolution

# for each picture provided in the config file...
for picture_params in pictures_to_paint:
    # load the config params for that picture
    with open(os.path.join(CONFIG_FOLDER, picture_params), 'r') as stream:
        config = yaml.safe_load(stream)
    # for each given configuration for the picture...
    if config:
        for element in config:
            # ... create a painting and upscale
            paint_from_config_params(element)

            # Optionally sleep for x minutes to cool down machine
            if SLEEP_IN_BETWEEN_PICTURES:
                print('Sleeping {} seconds before starting next painting...'.format(SLEEP_TIME_SECONDS))
                time.sleep(SLEEP_TIME_SECONDS)
