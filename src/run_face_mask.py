import os

content_img = 'olivia.jpg'
mask_img = 'olivia_mask.jpg'
# style_img = 'data_groen_2.jpg'
generated_image_1 = 'final_images/V5_CONTENT_olivia_STYLE_renoir_COLOR_False_final.png'

# output_name = 'DataInnovatieLabKPN_slight_nst'
generated_image_2 = 'final_images/V5_CONTENT_olivia_STYLE_renoir_COLOR_True_final.png'
content_weight = 10
style_weight = 1
iterations = 2

print('Applying face mask...')
apply_mask_cmd = "python mask_transfer.py ../data/output/{} ../data/output/{} ../data/inputs/mask/{}".format(generated_image_2, generated_image_1, mask_img)
os.system(apply_mask_cmd)
