import os
import random

from main.upscaling import upscaling


def load_styles_to_paint(STYLES_FOLDER):
    """DOC"""
    styles_to_paint = [doc for doc in os.listdir(STYLES_FOLDER)]
    return styles_to_paint


def paint_from_config_params(config_params):
    """DOC"""
    COLOR_PRESERVATION = config_params['preserve_color']
    if type(COLOR_PRESERVATION) is not list:
        COLOR_PRESERVATION = [COLOR_PRESERVATION]

    for col in COLOR_PRESERVATION:
        content_img = config_params['content_image']
        style_img = config_params['style_image']

        style = os.path.splitext(style_img)[0]
        content = os.path.normpath(os.path.splitext(content_img)[0]).split('\\')[-1]
        print('Painting {} using style {} and color preservation set to {}...'.format(content,
                                                                                      style,
                                                                                      col))

        if not os.path.exists("../data/output/{}".format(content)):
            os.mkdir("../data/output/{}".format(content))
            os.mkdir("../data/output/{}/final_images".format(content))

        content_weight = config_params['content_weight']
        style_weight = config_params['style_weight']
        iterations = config_params['iterations']
        image_size = config_params['image_size']
        init_image = config_params['init_image']
        pool_type = config_params['pool_type']
        preserve_color = col
        output_name = 'STYLE_{}_CONTENT_{}_COLOR_{}_IMSIZE_{}'.format(style, content, col, image_size)

        cmd = "python ./main/style_transfer/inetwork.py ../data/inputs/content/{} ../data/inputs/style/{} ../data/output/{}/{} --image_size {} --content_weight {} --style_weight {} --num_iter {} --init_image {} --pool_type {} --preserve_color {}".format(
            content_img, style_img, content, output_name, image_size, content_weight, style_weight, iterations,
            init_image, pool_type,
            preserve_color)
        os.system(cmd)

        # ... make the first and final image higher res
        if CONFIG_SETUP['apply_upscaling']:
            print('\nIncreasing resolution...')
            upscaler = upscaling.Upscaler()
            output_path = '../data/output/{}/{}'.format(content, output_name)
            first_image = os.path.split(output_path)[0] + '/final_images/' + os.path.split(output_path)[1] + "_first.png"
            final_image = os.path.split(output_path)[0] + '/final_images/' + os.path.split(output_path)[1] + "_final.png"
            for img in [first_image, final_image]:
                if os.path.isfile(img):
                    # set output path to same as input path; overwrite low res file by high res
                    upscaler.image_to_high_res(img, output_path=img)
        else:
            print('\nSkipping increase resolution step...')


# Point to folder containing the style images to use
STYLE_FOLDER = '../data/inputs/style/kansrijk_maar_niet_verkocht'
#STYLE_FOLDER = '../data/inputs/style/verkocht'

# Point to the content images, relative to main content folder
CONTENT_IMAGES = ['ROTTERDAM/rotterdam_erasmusbrug_vanaf_kade.jpg',
                  'ROTTERDAM/rotterdam_skyline_met_brug_overdag.jpg',
                  'ROTTERDAM/oude_hefbrug_cropped.jpg',
                  'ROTTERDAM/erasmus-bridge.jpg',
                  'ROTTERDAM/erasmus_brug_nacht.jpg',
                  'ROTTERDAM/witte_huis_cropped.jpg',
                  'ROTTERDAM/witte-de-with.jpg',
                  'ROTTERDAM/rotterdam-kubus-woningen.jpg',
                  'ROTTERDAM/markthal.jpg',
                  'ROTTERDAM/rotterdam-skyline-met-witte-huis.jpg',
                  'ROTTERDAM/rotterdam-erasmusbrug-2.jpg']

for image in CONTENT_IMAGES:
    # Setup the configuration
    CONFIG_SETUP = {'content_image': image,
                    'content_weight': 1,
                    'style_weight': 10,
                    'iterations': 2,
                    'image_size': 512,
                    'init_image': 'content',
                    'pool_type': 'ave',
                    'preserve_color': 'False',
                    'apply_upscaling': False}

    # Set whether to apply both WITH and WITHOUT color preservation
    both_color_variations = False

    # for each style provided
    styles = load_styles_to_paint(STYLE_FOLDER)
    random.shuffle(styles)
    for style_image in styles:
        CONFIG_SETUP['style_image'] = style_image
        try:
            # Paint with color preservation and without
            paint_from_config_params(CONFIG_SETUP)
            if both_color_variations:
                CONFIG_SETUP['preserve_color'] = not CONFIG_SETUP['preserve_color']
                paint_from_config_params(CONFIG_SETUP)
        except Exception as e:
            print(f'Skipping style: {style_image}\n{e}')
